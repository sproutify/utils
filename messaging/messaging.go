package messaging

import (
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/sproutify/utils/logger"
	"github.com/mitchellh/mapstructure"
	"github.com/nats-io/nats.go"
)

var NatsConnection *nats.Conn
var NatsEnConnection *nats.EncodedConn
var NatsRequestTimeout time.Duration = 20 * time.Millisecond

type NatsMessage struct {
	Data  interface{}
	Error string
}

/*
From the nats documentation:
// EncodedConn can Publish any raw Go type using the registered Encoder
"raw Go type" isn't a technical term as far as I can see, but it seems to mean types that don't need any additional library imports.
This is kind of an issue as time and timestamps aren't covered by this. time.Time types are handled by being converted to strings.

It's thus recommended that the type sent through in the Data interface be a json encoded string of the object. If you just send the object plain it will encode it ok,
but on the other side go won't interpret it as an struct but as a map[string]interface. This will require additional decoding using a map decoder, and so ultimately in terms
of simplicity you're basically back to where you started. Using gos built in json marshalling and unmarshalling makes sense though.
*/

//CreateEncodedConn will accept a nats host and port and set up a JSON encoded nats connection.
func CreateEncodedConn(host string, port string) {
	var err error
	logger.Debug("Connecting to Nats on " + host + ":" + port + "\n")

	NatsConnection, err = nats.Connect("nats://" + host + ":" + port)
	if err != nil {
		logger.Errorf("Can't connect to NATS. %s\n", string(err.Error()))
	}

	NatsEnConnection, err = nats.NewEncodedConn(NatsConnection, nats.JSON_ENCODER)
	if err != nil {
		logger.Errorf("Can't connect to JSON encoded NATS. %s\n", string(err.Error()))
	}
	if err == nil {
		logger.Debug("NATS connection successfully created\n")
	}
}

func SetRequestTimeout(millisecondTimeout time.Duration) {
	NatsRequestTimeout = millisecondTimeout
}

//This needs to be called in the microservice as a defer()
func CloseConnection() {
	NatsEnConnection.Close()
}

func Subscribe(subject string, callback func(NatsMessage) NatsMessage) {
	NatsEnConnection.Subscribe(subject, func(subj, reply string, natsMessage NatsMessage) {
		logger.Debugf("Nats listener has received a message on this subject: %v", subject)
		if err := NatsEnConnection.Publish(reply, callback(natsMessage)); err != nil {
			logger.Errorf("Failed to send publish response to nats on subject: %v, Reply Message: %v, Error Mesage: %v\n", subject, natsMessage, err.Error())
		}
	})
}

func Request(subject string, request NatsMessage) NatsMessage {
	var response NatsMessage
	if err := NatsEnConnection.Request(subject, request, &response, NatsRequestTimeout); err != nil {
		errorString := fmt.Sprintf("Nats Request connection failure on this subject %v, Request Message: %v, Timeout Time: %v, Error Mesage: %v", subject, request, NatsRequestTimeout, err.Error())
		logger.Errorf(errorString)
		response.Error = errorString
		return response
	} else {
		return response
	}
}

// func dbReadSubscribe(subject string, callback func(uint) ) {

// }

//DBread takes in a callback(that expects a uint), and returns a function that is accepted by subscribe (Nats message in, nats message out)
func DbRequestByID(callback func(uint) (interface{}, error)) func(NatsMessage) NatsMessage {

	return func(natsMessage NatsMessage) NatsMessage {
		if inputIdFloat, inputIdOK := natsMessage.Data.(float64); inputIdOK {
			inputId := uint(inputIdFloat)
			logger.Debugf("Received an id as part of the read function: %d\n", uint(inputId))
			if returnObject, err := callback(inputId); err != nil {
				return NatsMessage{nil, err.Error()}
			} else {
				return NatsMessage{returnObject, ""}
			}
		} else {
			return NatsMessage{nil, "Data not OK (expecting a number)"}
		}
	}

}

// func dbRequestByEntry(callback func(dbInputStruct) (dbInputStruct, error), inputType reflect.Type) func(NatsMessage) NatsMessage {

// 	return func(natsMessage NatsMessage) NatsMessage {
// 		if inputEntry, inputIdOK := dbInputStruct.dbInputStruct(); inputIdOK {
// 			inputId := uint(inputIdFloat)
// 			logger.Debugf("Received an id as part of the read function: %d\n", uint(inputId))
// 			if returnObject, err := callback(inputId); err != nil {
// 				return NatsMessage{nil, err.Error()}
// 			} else {
// 				return NatsMessage{returnObject, ""}
// 			}
// 		} else {
// 			return NatsMessage{nil, "Struct not ok"}
// 		}
// 	}

// }

//https://github.com/mitchellh/mapstructure/issues/159
func ToTimeHookFunc() mapstructure.DecodeHookFunc {
	return func(f reflect.Type, t reflect.Type, data interface{}) (interface{}, error) {
		if t != reflect.TypeOf(time.Time{}) {
			return data, nil
		}

		switch f.Kind() {
		case reflect.String:
			return time.Parse(time.RFC3339, data.(string))
		case reflect.Float64:
			return time.Unix(0, int64(data.(float64))*int64(time.Millisecond)), nil
		case reflect.Int64:
			return time.Unix(0, data.(int64)*int64(time.Millisecond)), nil
		default:
			return data, nil
		}
		// Convert it by parsing
	}
}

func Decode(input map[string]interface{}, result interface{}) error {
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Metadata: nil,
		DecodeHook: mapstructure.ComposeDecodeHookFunc(
			ToTimeHookFunc()),
		Result: result,
	})
	if err != nil {
		return err
	}

	if err := decoder.Decode(input); err != nil {
		return err
	}
	return err
}
