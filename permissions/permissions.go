package permissions

type Permission struct {
	 Permission  string
	 Description string
}