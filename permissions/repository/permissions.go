package repository

import "bitbucket.org/sproutify/utils/permissions"

type PermissionRepository interface {
	query(id string) []permissions.Permission
	init()
}

