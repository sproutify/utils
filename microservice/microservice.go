package microservice

import (
	"bitbucket.org/sproutify/utils/db"
	"bitbucket.org/sproutify/utils/logger"
	"bitbucket.org/sproutify/utils/messaging"
	"bitbucket.org/sproutify/utils/permissions"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	"os"
	"strings"
)

type requestHandler func(service *Microservice, message Request)(response *Response)

type Microservice struct {
	ServiceName string
	Route       string
	Timeout     uint
	Handles     map[string]RequestHandler
	DB          *gorm.DB
	Logger      *os.File
}

type RequestHandler struct {
	handle      string
	handler     func(service *Microservice, message Request)(response *Response)
	permissions []permissions.Permission
}

func Create(config string, configPath string, logFile *string)(service *Microservice){

	viper.SetConfigName(config) // name of config file (without extension)
	viper.SetConfigType("json") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(configPath) // path to look for the config file in
	viper.AddConfigPath(".")        // optionally look for config in the working directory
	err := viper.ReadInConfig()     // Find and read the config file

	if err != nil {                 // Handle errors reading the config file
		//logger.Fatalf("Fatal error config file: %s \n", err)
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	var details = db.DB{
		DbType:   viper.GetString("database.dbtype"),
		Host:     viper.GetString("database.host"),
		Port:     viper.GetString("database.port"),
		User:     viper.GetString("database.user"),
		DbName:   viper.GetString("database.dbname"),
		Password: viper.GetString("database.password"),
		SslMode:  viper.GetString("database.sslmode"),
	}

	db := db.Connect(details)

	return &Microservice{
		ServiceName: viper.GetString("microservice.service"),
		Route:       viper.GetString("microservice.route"),
		Timeout:     uint(viper.GetInt("microservice.timeout")),
		Handles:     map[string]RequestHandler{},
		Logger:      logger.CreateLogger(*logFile+".log", viper.GetString("logging.level")),
		DB:          db,
	}
}

func (service *Microservice) AddHandler(route string, handler func(service *Microservice, message Request)(response *Response), permissions []permissions.Permission)(s *Microservice){
	var handle = strings.Replace(route, "/", "", -1)
	service.Handles[handle] = RequestHandler{handle, handler, permissions}
	return service
}

/**
	Goal of the Microservice is to Register with the APIGW and handle requests to the
	Indicated Route.

	//TODO: handle transformation from NatsMessage to Response
 */
func (service *Microservice) Run(init func(service *Microservice)) (s *Microservice) {

	init(service)

	messaging.CreateEncodedConn(viper.GetString("nats.host"), viper.GetString("nats.port"))

	var request = messaging.NatsMessage{
		Data:  map[string]interface{}{
			"Route": service.Route,
			"Timeout": service.Timeout,
			"ServiceName": service.ServiceName,
		},
		Error: "",
	}

	if response := messaging.Request("register", request); response.Error == "" {
		logger.Info("Microservice Registered Successfully")
		for handle, _ := range service.Handles {
			var subject = service.Route + "." + handle

			logger.Info("Subscribing to Subject:", subject)
			messaging.Subscribe(subject, func(message messaging.NatsMessage) messaging.NatsMessage {
				//TODO: handle delegation to the handler (converting from NatsMessage to Request & Response)
				logger.Debug("Message Received for Subject:", subject)
				return messaging.NatsMessage{}
			})
		}
	} else {
		//TODO: panic
	}

	return service
}

func (service *Microservice) Shutdown(){
	service.DB.Close()
	service.Logger.Close()
}