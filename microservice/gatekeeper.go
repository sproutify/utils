package microservice

import (
	"bitbucket.org/sproutify/utils/permissions"
	"bitbucket.org/sproutify/utils/permissions/repository"
)

type APIGateKeeper struct {
}

func (keeper *APIGateKeeper) Authorize( authToken string,
										repository repository.PermissionRepository,
										requiredPermissions []permissions.Permission,
										handler requestHandler)(res *Response){
	return &Response{}
}