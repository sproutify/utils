package logger

import (
	"github.com/op/go-logging"
	"os"
	"strings"
)

var log *logging.Logger

//logPath = logs/ms-ihss-induction-db.log
func CreateLogger(logPath string, logLevel string) *os.File {

	log = logging.MustGetLogger("example")
	log.ExtraCalldepth = 1
	var format = logging.MustStringFormatter(
		//`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
		//`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.8s} %{color:reset} %{message}`,
		`%{time:2006-01-02 15:04:05.000} %{shortfunc} ▶ %{level:.8s} %{message} %{shortfile}`,
	)

	logFile, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Error("Can't create log file")
	}

	logFileBackend := logging.NewLogBackend(logFile, "", 0)
	consoleLogBackend := logging.NewLogBackend(os.Stderr, "", 0)

	consoleLogFormatted := logging.NewBackendFormatter(consoleLogBackend, format)

	backendLeveled := logging.AddModuleLevel(logging.NewBackendFormatter(logFileBackend, format))

	logging.SetBackend(backendLeveled, consoleLogFormatted)

	switch strings.ToUpper(logLevel) {
	case "DEBUG":
		backendLeveled.SetLevel(logging.DEBUG, "")
	case "INFO":
		backendLeveled.SetLevel(logging.INFO, "")
	case "NOTICE":
		backendLeveled.SetLevel(logging.NOTICE, "")
	case "WARNING":
		backendLeveled.SetLevel(logging.WARNING, "")
	case "ERROR":
		backendLeveled.SetLevel(logging.ERROR, "")
	case "CRITICAL":
		backendLeveled.SetLevel(logging.CRITICAL, "")
	case "DEFAULT":
		backendLeveled.SetLevel(logging.DEBUG, "")
	}

	return logFile
}

//Create function wrappers around the logger, so that 'log' can be a private variable
func Debug(args ...interface{}) {
	log.Debug(args)
}
func Debugf(format string, args ...interface{}) {
	log.Debugf(format, args)
}
func Info(args ...interface{}) {
	log.Info(args)
}
func Infof(format string, args ...interface{}) {
	log.Infof(format, args)
}
func Notice(args ...interface{}) {
	log.Notice(args)
}
func Noticef(format string, args ...interface{}) {
	log.Noticef(format, args)
}
func Warning(args ...interface{}) {
	log.Warning(args)
}
func Warningf(format string, args ...interface{}) {
	log.Warningf(format, args)
}
func Error(args ...interface{}) {
	log.Error(args)
}
func Errorf(format string, args ...interface{}) {
	log.Errorf(format, args)
}
func Critical(args ...interface{}) {
	log.Critical(args)
}
func Criticalf(format string, args ...interface{}) {
	log.Criticalf(format, args)
}
func Fatal(args ...interface{}) {
	log.Fatal(args)
}
func Fatalf(format string, args ...interface{}) {
	log.Fatalf(format, args)
}
